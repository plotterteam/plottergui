package frazeusz.gui;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

public class MainGUI extends JFrame{
	private JPanel searchPanel;
	private JTabbedPane tabs;
	private MainGUIController controller;
	private JButton stopButton;
	
	public MainGUI(MainGUIController controller, String name){
		super(name);
		this.controller = controller;
		searchPanel = new SearchPanel(controller.getPatternMatcherGUI(), 
										controller.getCrawlerGUI().getRootPanel());
		
		
		setLayout(new BorderLayout());
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		tabs = new JTabbedPane();
		tabs.addTab("Wyszukiwanie", searchPanel);
		tabs.addTab("Wyniki", controller.getPlotterGUI());
		tabs.addTab("Zapisz/Wczytaj", controller.getWriterGUI());
		add(tabs);
		pack();
		setVisible(true);
	}
	
	public class SearchPanel extends JPanel implements ActionListener{
		private JButton startButton;
		
		public SearchPanel(JPanel patternMatcherGUI, JPanel crowlerGUI){
			setLayout(new BorderLayout());
			startButton = new JButton("Start");
			JPanel panel = new JPanel();
			panel.setLayout(new BorderLayout());
			panel.add(patternMatcherGUI, BorderLayout.WEST);
			panel.add(crowlerGUI, BorderLayout.CENTER);
			add(panel, BorderLayout.CENTER);
			panel = new JPanel();
			panel.setLayout(new GridLayout(1,2));
			panel.add(startButton);
			startButton.addActionListener(this);
			stopButton = new JButton("Stop");
			panel.add(stopButton);
			stopButton.addActionListener(this);
			add(panel, BorderLayout.SOUTH);
		}
		
		public void actionPerformed(ActionEvent e){
			if (e.getSource() == startButton && !controller.getIsCrawling()){
				try {
					controller.startSearching();
					tabs.addTab("Wydajność", controller.getMonitor().getPanel());
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				
			}
			else if (e.getSource() == stopButton){
				controller.stopCrawling();
			}

		}
	}
	public void showCrawlingFinishedDialog(){
		JOptionPane.showMessageDialog(this, "Crawling finished!", "Info", JOptionPane.INFORMATION_MESSAGE);
	}
	
}
