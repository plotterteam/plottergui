package frazeusz.gui;

import java.util.List;

import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import search.NLProcessor;
import frazeusz.crawler.Crawler;
import frazeusz.crawler.CrawlerGUI;
import frazeusz.monitor.Monitor;
import frazeusz.parser.Parser;
import frazeusz.patternMatcher.PatternMatcher;
import frazeusz.patternMatcher.PatternMatcherGUI;
import frazeusz.plotter.IPlotter;
import frazeusz.plotter.Plotter;
import frazeusz.plotter.PlotterGUI;
import frazeusz.writer.WriterAPI;
import frazeusz.writer.WriterGUI;

public class MainGUIController {
	private boolean isCrawling = false;
	private Crawler crawler;
	private NLProcessor nlp;
	private Parser parser;
	private PatternMatcher patternMatcher;
	private Plotter plotter;
	private WriterAPI writerAPI;
	
	private CrawlerGUI crawlerGUI;
	private Monitor monitor;
	private PlotterGUI plotterGUI;
	private WriterGUI writerGUI;
	private PatternMatcherGUI patternMatcherGUI;
	private MainGUI mainGUI;
	
	public MainGUIController(){
		try {
			nlp = new NLProcessor();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("Could not initialize NLProcessor");
		}
		crawlerGUI = new CrawlerGUI();
		patternMatcherGUI = new PatternMatcherGUI();
		plotterGUI = new PlotterGUI();
		plotter = new Plotter(plotterGUI);
		writerAPI = new WriterAPI();
		setWriterGUI(new WriterGUI(null, plotter, writerAPI));
	}
	public void startMainGUI(){
		this.mainGUI = new MainGUI(this, "Crawler");
	}

	public CrawlerGUI getCrawlerGUI() {
		return crawlerGUI;
	}

	public void setCrawlerGUI(CrawlerGUI crawlerGUI) {
		this.crawlerGUI = crawlerGUI;
	}

	public PatternMatcherGUI getPatternMatcherGUI() {
		return patternMatcherGUI;
	}

	public void setPatternMatcherGUI(PatternMatcherGUI patternMatcherGUI) {
		this.patternMatcherGUI = patternMatcherGUI;
	}

	public Monitor getMonitor() {
		return monitor;
	}

	public void setMonitor(Monitor monitor) {
		this.monitor = monitor;
	}

	public PlotterGUI getPlotterGUI() {
		return plotterGUI;
	}

	public void setPlotterGUI(PlotterGUI plotterGUI) {
		this.plotterGUI = plotterGUI;
	}

	public JPanel getWriterGUI() {
		return writerGUI;
	}



	public void startSearching() throws Exception {
		
		patternMatcher = new PatternMatcher(patternMatcherGUI.getPhrases(), nlp);
		writerGUI.setPatternMatcher(patternMatcher);
		parser = new Parser(patternMatcher);
		
		crawler = new Crawler( crawlerGUI.getConfigurator(), parser);
		monitor = new Monitor(crawler.getStatistics(), patternMatcher);
		plotter.setPatternMatcher(patternMatcher);
		plotter.setDataSize(patternMatcherGUI.getPhrases().size());
		
		Thread crawlerThread = new Thread(){
			public void run(){
				crawler.start();
				mainGUI.showCrawlingFinishedDialog();
			}
		};
		isCrawling = true;
		crawlerThread.start();
		
		monitor.startMonitor();
		System.out.println("monitor started");
	}

	public Plotter getPlotter() {
		return (Plotter) plotter;
	}

	public void setPlotter(Plotter plotter) {
		this.plotter = plotter;
	} 
	public NLProcessor getNlProcessor(){
		return this.nlp;
	}
	public WriterAPI getWriterAPI() {
		return writerAPI;
	}
	public void setWriterGUI(WriterGUI writerGUI) {
		this.writerGUI = writerGUI;
	}
	public void stopCrawling(){
		crawler.stopCrawling();
		isCrawling = false;
	}
	public boolean getIsCrawling(){
		return this.isCrawling;
	}
}
