package frazeusz.plotter;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.Scrollbar;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.data.category.DefaultCategoryDataset;

import javax.swing.ButtonGroup;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollBar;
import javax.swing.JSlider;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;


public class PlotterGUI extends JPanel implements ActionListener, AdjustmentListener{
	private int firstItemIndex;
	private int visibleItemsCount;
	
	private ChartPanel chartPanel;
	private PlotOrientation plotOrientation;
	private IPlotter plotter;
	private JScrollBar scrollBar;

	private JLabel orientationLabel, itemsCountLabel;
	private JRadioButton horizontalButton, verticalButton;
	private ButtonGroup orientationButtonGroup;
	private JComboBox<Integer> itemsCountComboBox;
	
	public PlotterGUI(){
		super(new BorderLayout());
		visibleItemsCount = 4;
		JPanel viewOptionsPanel = new JPanel();
		orientationLabel = new JLabel("Orientacja:");
		horizontalButton = new JRadioButton("pozioma", false);
		verticalButton = new JRadioButton("pionowa", true);
		orientationButtonGroup = new ButtonGroup();
		orientationButtonGroup.add(verticalButton);
		orientationButtonGroup.add(horizontalButton);
		itemsCountLabel = new JLabel("Widocznych kolumn:");
		itemsCountComboBox = new JComboBox<Integer>(makeRange(10));
		itemsCountComboBox.setSelectedIndex(visibleItemsCount - 1);
		itemsCountComboBox.addActionListener(this);
		
		viewOptionsPanel.setLayout(new GridLayout(1, 5));
		viewOptionsPanel.add(orientationLabel);
		viewOptionsPanel.add(verticalButton);
		viewOptionsPanel.add(horizontalButton);
		viewOptionsPanel.add(itemsCountLabel);
		viewOptionsPanel.add(itemsCountComboBox);
		add(viewOptionsPanel, BorderLayout.NORTH);
		horizontalButton.addActionListener(this);
		verticalButton.addActionListener(this);
		
		plotOrientation = PlotOrientation.VERTICAL;
		chartPanel = new ChartPanel(null);
		chartPanel.setPopupMenu(null);
		scrollBar = new JScrollBar(JScrollBar.HORIZONTAL, 0, visibleItemsCount, 0, visibleItemsCount);
		
		//scrollBar = new JScrollBar(JScrollBar.HORIZONTAL, 0, visibleItemsCount, 0, Math.max(0, visibleItemsCount));
		//scrollBar = new JScrollBar(JScrollBar.HORIZONTAL, 0, visibleItemsCount, 0, Math.max(0, plotter.getDataSize() - visibleItemsCount));
		scrollBar.setBlockIncrement(1);
		scrollBar.addAdjustmentListener(this);
		add(scrollBar, BorderLayout.SOUTH);
		add(chartPanel);
		firstItemIndex = 0;
		
		
		
	}
	
	public void updateView(){
		int dataSize = plotter.getDataSize();
		scrollBar.setMaximum(Math.max(0, dataSize));
		scrollBar.setVisibleAmount(Math.min(visibleItemsCount, dataSize));
		
		DefaultCategoryDataset dataset = plotter.getViewDataset(firstItemIndex, visibleItemsCount);
		//dataset.getRowCount();
		JFreeChart chart = ChartFactory.createBarChart3D("Znalezione frazy",
				null, null, dataset, plotOrientation,
				false, true, false);
		BarRenderer renderer = (BarRenderer)chart.getCategoryPlot().getRenderer();
		renderer.setMaximumBarWidth(.25);
		chart.getCategoryPlot().getRangeAxis().setRange(0, plotter.getMaxValue() + 10);
		chartPanel.setChart(chart);
		
	}
    
    public void adjustmentValueChanged(AdjustmentEvent e){
    	int value = this.scrollBar.getValue();
        firstItemIndex = value;
        updateView();
	}
    
    public void actionPerformed(ActionEvent e){
    	Object source = e.getSource();
    	if(source == horizontalButton){
    		plotOrientation = PlotOrientation.HORIZONTAL;
    		scrollBar.setOrientation(JScrollBar.VERTICAL);
    		add(scrollBar, BorderLayout.EAST);
    	}
    	else if(source == verticalButton){
    		plotOrientation = PlotOrientation.VERTICAL;
    		scrollBar.setOrientation(JScrollBar.HORIZONTAL);
    		add(scrollBar, BorderLayout.SOUTH);
    	}
    	else if(source == itemsCountComboBox){
    		visibleItemsCount = (int)((JComboBox<Integer>)source).getSelectedItem();
    	}
    	updateView();
    }
    
    public void setPlotter(IPlotter plotter){
    	this.plotter = plotter;
    	
    	//scrollBar.setMaximum(Math.max(0, plotter.getDataSize() - visibleItemsCount));
    }
    
    /*public void setDataSize(int size){
    	this.
    	scrollBar.setMaximum(Math.max(0, size - visibleItemsCount));
    }*/
    
    private Integer[] makeRange(int max){
    	Integer[] ints = new Integer[max];
    	for(int i=0; i<max; ++i)
    		ints[i] = i+1;
    	return ints;
    }
}
