package frazeusz.plotter;

import java.util.Map;

import org.jfree.data.category.DefaultCategoryDataset;

import frazeusz.patternMatcher.IPatternMatcherResults;
import frazeusz.patternMatcher.Phrase;


public interface IPlotter {
	public void updateChart(Map<Phrase, Integer> searchResult);
	public void setPatternMatcher(IPatternMatcherResults patternMatcher);
	public DefaultCategoryDataset getViewDataset(int first, int size);
	public int getDataSize();
	//public void setDataSize(int count);
	public int getMaxValue();
}
