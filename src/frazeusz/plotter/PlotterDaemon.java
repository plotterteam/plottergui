package frazeusz.plotter;


public class PlotterDaemon implements Runnable{
	private Plotter plotter;
	private final int chartUpdateTime = 2000; //Milliseconds
	
	public PlotterDaemon(Plotter plotter){
		this.plotter = plotter;
		}

	@Override
	public void run() {
		while(true){
			//System.nieout.println(plotter);
			plotter.updateChart();
			try {
				Thread.sleep(chartUpdateTime);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
	

}
