package frazeusz.plotter;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.theories.Theories;
import static org.easymock.EasyMock.*;

import frazeusz.gui.MainGUI;

public class PlotterTest {
	private Plotter plotter;
	private PlotterGUI plotterGUImock;

	@Before
	public void setUpBeforeClass() throws Exception {
		plotterGUImock= createMock(PlotterGUI.class);
		replay(plotterGUImock);
		plotter = new Plotter(plotterGUImock);
	}

	@Test
	public void testPlotterReturnsNotNull() {
		assertNotNull(plotter);
	}

}
