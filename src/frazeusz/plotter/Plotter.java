package frazeusz.plotter;

import java.util.AbstractMap;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.jfree.data.category.DefaultCategoryDataset;

import search.WordVariant;
import frazeusz.patternMatcher.IPatternMatcherResults;
import frazeusz.patternMatcher.Phrase;

public class Plotter implements IPlotter{
	private IPatternMatcherResults patternMatcher;
	private PlotterGUI plotterGUI;
	private List<Map.Entry<String, Integer>> dataList;
	private int dataSize;
	private int maxValue;

	public Plotter(PlotterGUI plotterGUI) {
		this.plotterGUI = plotterGUI;
		plotterGUI.setPlotter(this);
		dataList = new LinkedList<Map.Entry<String, Integer>>();
	}

	public void updateChart(){
		Map<Phrase, Integer> results = patternMatcher.getResults();
		updateChart(results);
	}
	
	public DefaultCategoryDataset getViewDataset(int first, int size){
		DefaultCategoryDataset dataset = new DefaultCategoryDataset();
		List<Map.Entry<String, Integer>> sublist = dataList.subList(first, Math.min(first + size, dataList.size()));
				
		for(Map.Entry<String, Integer> entry : sublist){
			
			String phraseText = entry.getKey();
			Integer value = entry.getValue();
			dataset.addValue(value, "", phraseText);
		}
		
		return dataset;
	}
	
	public void updateDataList(Map<Phrase, Integer> results) {
		int max = 0;
		
		List<Map.Entry<String, Integer>> resultsList = new LinkedList<Map.Entry<String, Integer>>();
		for(Map.Entry<Phrase, Integer> entry : results.entrySet()){
			WordVariant variant = entry.getKey().getWordVariant();
			String variantString = "<";
			if(variant.searchForSynonyms()) variantString += "s";
			if(variant.searchForInflections()) variantString += "o";
			if(variant.searchForDiminutives()) variantString += "z";
			variantString += ">";
			String phraseText = entry.getKey().getText() + " "+ variantString; 
			Integer value = entry.getValue();
			max = Math.max(max, value);
			resultsList.add(new AbstractMap.SimpleEntry<String, Integer>(phraseText, value));
		}
		
		maxValue = max;
		
		Collections.sort(resultsList, new Comparator<Map.Entry<String, Integer>>() {
			public int compare(Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2) {
				return -(o1.getValue()).compareTo(o2.getValue());
			}
		});
	
		dataList = resultsList;
	}
	
	
	public void updateChart(Map<Phrase, Integer> results) {
		updateDataList(results);
		plotterGUI.updateView();
	}
	
	public void setPatternMatcher(IPatternMatcherResults patternMatcher){
		this.patternMatcher = patternMatcher;
		if(patternMatcher != null)
			new Thread(new PlotterDaemon(this)).start();
		
	}
	
	public IPatternMatcherResults getPatternMatcher(){
		return this.patternMatcher;
	}
	
	public int getDataSize(){
		//return dataList.size(); //dataSize;
		return dataSize;
	}
	
	public void setDataSize(int size){
		dataSize = size;
		//plotterGUI.setDataSize(size);
	}
	
	public int getMaxValue(){
		return maxValue;
	}

}
